<?php
class mfXfront
    extends mfFront
{
    

    public function __construct() { parent::__construct(null); }


	public function clearSessionFilter($params)
	{
	
		$_SESSION['lastFilter']=array();
	}

	
	public function tester($params)
	{		    
		$c=3+4;
		$this->result['c']=$c;		
	}

    public function addMassItems($params)
    {

        $ishop = xCore::moduleFactory('ishop.front');

        if(isset($params['massItems']))
        {


            foreach($params['massItems'] as $item)
            {
                if($item['count']==0)
                {
                    unset($ishop->cartStorage[$item['id']]);
                }else
                {	unset($ishop->cartStorage[$item['id']]);
                    $ishop->addToCart($item['id'], $item['count'], true);
                }
            }

            $this->result['cartStorage']=$ishop->cartStorage->get();

        }

    }





    public function proxyAjax($params)
    {

        $entityBody = file_get_contents('php://input');
        $entityBody=unserialize($entityBody);
        $entityBody['arguments']=unserialize($entityBody['arguments']);
        if($argVal=$entityBody['arguments'][0])
        {
            $argKey=key($argVal);
            $entityBody['arguments'][0][$argKey]=$argVal[$argKey]['proxyAjax'];


            $entityBody['arguments']=serialize($entityBody['arguments']);
            $entityBody=serialize($entityBody);


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,            "http://x4.bi/?xoadCall=true&recall=true" );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt($ch, CURLOPT_POST,           1 );
            curl_setopt($ch, CURLOPT_POSTFIELDS,     "$entityBody");
            curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain'));
            if(!$_GET['recall'])
            {
                $result=curl_exec($ch);
                echo $result;
                die();
            }

        }


    }

    public function addSpecialToCart($params)
    {
        
             
         $catObject = $this->_module->_tree->getNodeInfo($params['id']);         
         $objectInfo = $this->_module->_commonObj->convertToPSG($catObject);
         $ishop=xCore::moduleFactory('ishop.front');                              
         $this->result['lastAdded']=$ishop->addToCart($params['id'],1,false,array('outerPrice'=>$objectInfo['specials']['customerPrice']['value']));
              
    }


    public function getRecommendedForSku($params)
    {

        $node=$this->_module->_sku->getNodeInfo($params['id']);

        if ($node['params']['connected'])
        {
            $node['params']['connected']=str_replace(' ', '', $node['params']['connected']);
            $pairs                      =explode(';', $node['params']['connected']);

            foreach ($pairs as $pair)
            {
                $colors = explode(',', $pair);

                if ($nodes=$this->_module->_sku->selectStruct('*')->selectParams('*')->where(array
                (
                    'akey',
                    '=',
                    implode('_', $colors)
                ))->limit(0, 1)->run())
                {
                    $recommend[]=$nodes[0];
                }
            }


            foreach($recommend as &$rec)
            {
                $rec['link']= $this->_module->_commonObj->buildLink($rec['netid'],8714);

            }

            $this->result['items']=$recommend;
        }
    }

    public function saveOrderProjectForm($params)
	{
        $this->_module->loadModuleTemplate('order_project_form_sendmail.html');
        $this->_module->_TMS->addMassReplace('mail_body', $params['data']);

        $m=xCore::incModuleFactory('Mail');           
        $m->From('info@avex.by');
        $m->To(array('info@avex.by','task_0ae29c1e2ad2882d9@worksection.com'));
        $m->Content_type('text/html');
        $m->Subject($this->_module->_TMS->parseSection('mail_subject'));

        $dataKeys = array_keys($params['data']);
        foreach($dataKeys as $key){
            if(strstr($key, 'filename')){
                $m->Attach('uploads/'.$params['data'][$key]);
            }
        }

        $m->Body($this->_module->_TMS->parseSection('mail_body'),xConfig::get('GLOBAL','siteEncoding'));
        $m->Priority(2);
        $m->Send();
    }
    
    public function saveRepairForm($params){
		
		
        //debugbreak();       
        if($params['data']['other_brand'] != ''){
           $params['data']['brand'] = $params['data']['other_brand']; 
        }
        
        $this->_module->loadModuleTemplate('repair_form_sendmail.html');
        $this->_module->_TMS->addMassReplace('mail_body', $params['data']);

        $this->_module->_TMS->addMassReplace('mail_body_xls', $params['data']);
        $xls=$this->_module->_TMS->parseSection('mail_body_xls');
        
        
        $fname=md5(print_r($params['data'],true));
        $media=  xConfig::get('PATH','MEDIA').'remont/'.$fname.'.xls';
        $fp = fopen($media, 'w');
        fwrite($fp, $xls);
        
        $m=xCore::incModuleFactory('Mail');
        $m->From(xConfig::get('GLOBAL','admin_email'));
		$m->To(array('task_0ae29c1e2ad2882d9@worksection.com','info@avex.by'));
		
        //$m->To(array(xConfig::get('GLOBAL','admin_email'),'task_0ae29c1e2ad2882d9@worksection.com'));
        $m->Content_type('text/html');
        $m->Subject($this->_module->_TMS->parseSection('mail_subject'));
        $m->Attach($media);
        
        $m->Body($this->_module->_TMS->parseSection('mail_body'),xConfig::get('GLOBAL','siteEncoding'));
        $m->Priority(2);
        $r = $m->Send();
    }

   
    public function saveContactForm($params){
        $this->_module->loadModuleTemplate('contact_form_sendmail.html');
        $this->_module->_TMS->addMassReplace('mail_body', $params['data']);

        $m=xCore::incModuleFactory('Mail');

        $m->From(xConfig::get('GLOBAL','admin_email'));
        $m->To(xConfig::get('GLOBAL','admin_email'));
        $m->Content_type('text/html');
        $m->Subject($this->_module->_TMS->parseSection('mail_subject'));
        $m->Body($this->_module->_TMS->parseSection('mail_body'),xConfig::get('GLOBAL','siteEncoding'));
        $m->Priority(2);
        $m->Send();
    }



}
?>
