$(document).ready(function(){

    var catalog=x4.getModule('catalogFront');
    var pages=x4.getModule('pagesFront');
    
    function renderFilterResults(formid)
    {
                serialized=$('#'+formid).serialize();
                serialized=decodeURIComponent(serialized);                				
				index=location.pathname.indexOf('/--');
				
				if(index!=-1)
					{
						pathname=location.pathname.slice(0 , index);
						
					}else{
					
						pathname=location.pathname;
					}
					
                gpath=location.host+pathname+'/?'+serialized;								
                gpath=gpath.replace(/\/\//g,"/");
				
				 
				
			var     url=location.protocol + '//' + gpath;

            $('.catalogListHolder').css('opacity',0.5);
            res=pages.renderSlot(url,['middle','filter'],
            function(xres,bres)
            {
                slots=bres.result.slots;

                $('.catalogListHolder').replaceWith(slots['middle']);
                $('.catalogListHolder').css('opacity',1);
                $('.filterHolder').parent().replaceWith(slots['filter']);
                sliderRender();
    			 catalog.execute({buildUrlTransformation:{url:url}});
                 window.history.replaceState("object or string", "Title",catalog.connector.result.url);


            }
        );
    }




    $('.onPriceClick').click(function(e){
        e.preventDefault();
        $('.priceMin').val($(this).data('min'));
        $('.priceMax').val($(this).data('max'));

        renderFilterResults('mainFilter');

    });

    $('.catalogCurrentComparseCount').catalogCurrentComparseCount({});



    $('.addToComparse').catalogAddComparse({

            countContainer: '.catalogCurrentComparseCount',
            onAdded:()=>
            {
                swal({
                    title: "Bra jobbat!",
                    text: "produkt tillsatt för jämförelse",
                    icon: "success",
                    button: "ok"
                });
            },

        onBeforeAdd:()=>{
            return true;
        }

    });


    function intelliRoundUp(num)
    {
        var len=(num+'').length;
        var fac=Math.pow(10,len-1);

        return Math.ceil(num/fac)*fac;
    }

    function intelliRoundDown(num)
    {
        var len=(num+'').length;
        var fac=Math.pow(10,len-1);

        return Math.floor(num/fac)*fac;
    }


    function sliderRender(){
        $(".slider-range").each(function()
        {
            var pl=$(this).parent().find(".price_low");
            var ph=$(this).parent().find(".price_high");
            var pl_real=$(this).parent().find(".price_low_real");
            var ph_real=$(this).parent().find(".price_high_real");

            plv=parseInt(pl.val());
            phv=parseInt(ph.val());
            plv=intelliRoundDown(plv);
            phv=intelliRoundUp(phv);

            ipl=pl.data('infiltermin');
            iph=ph.data('infiltermax');

            if(plv>100)plv=plv-100;
            if(!ipl)ipl=plv;
            if(!iph)iph=phv;


            slider=noUiSlider.create($(this).get(0),
                {
                    start:[ipl,iph],
                    behaviour: 'tap',

                    range: {
                        min:[plv,100],
                        max:phv
                    }

                });

            $(this).get(0).noUiSlider.on('change', function()
            {
                renderFilterResults('mainFilter');
            });

            $(this).get(0).noUiSlider.on('update', function( values, handle ) {
                pl_real.val(Math.round(values[0]));
                pl.val(
                    accounting.formatMoney(Math.round(values[0]), {
                        format: "%v %s",
                        symbol:'',//pl.data('currency'),
                        thousand: " ",
                        precision: 0
                    })
                );

                ph_real.val(Math.round(values[1]));
                ph.val(
                    accounting.formatMoney(Math.round(values[1]), {
                        format: "%v %s",
                        symbol:'',//ph.data('currency'),
                        thousand: " ",
                        precision: 0
                    })
                );
            });
        });


    }

    sliderRender();


    $('#mainFilter .reset_filter_btn').click(function(){
		catalog.execute({clearSessionFilter:true});
	});

    $(document).on("change",'#sortMenu',function(){document.location.href=$(this).val();});

    $(document).on("click", "#mainFilter input", function(e)
    {
        renderFilterResults('mainFilter');
    });

    

   
});

