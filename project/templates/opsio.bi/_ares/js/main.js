

$(document).ready(function() {

    $(document).foundation();
    
    /*select custom*/
    $('select').styler();

    $('select').change(function() {
        var value = $(this).val();
        $(this).find('option').each(function() {
            if ( $(this).val() == value ) $(this).prop('disabled', true);
        });
        document.location.href=value;
        $('select').trigger('refresh');

    });

    $('.how_much_block input').styler();
    $('.types_payment_table input').styler();
    $('.cart_table input[type=number]').styler();
    /*END select custom*/
    

    /*Carousel*/
        $(".main_corousel").owlCarousel({
            loop:true,
            items: 1,
            dots: true,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true
        });

        $(".main_catalog_slider").owlCarousel({
            loop:true,
            items: 1,
            dots: true,
            autoplay: false,
            autoplayTimeout: 4000,
            autoplayHoverPause: true
        });

        $(".main_product_small_slider").owlCarousel({
            loop:true,
            items: 3,
            dots: false,
            autoplay: false,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            nav: true,
            navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"]
        });

        $(".card_product_small_slider").owlCarousel({
            loop:true,
            items: 6,
            dots: false,
            autoplay: false,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            nav: true,
            navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"]
        });

        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
            infinite: false,
            mobileFirst: true
        });

        $('.slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            infinite: false,
            mobileFirst: true,
            dots: false,
            arrows: false,
            focusOnSelect: true,
            vertical: true
        });
    /*END Carousel*/


    /*====== Tooltip =====*/
    $('.tip_block .tip').on('click', function(){
        $(this).siblings().toggle();
        $(this).toggleClass('active');
        $(this).find('i').toggleClass('ion-ios-plus-empty');
        $(this).find('i').toggleClass('ion-ios-minus-empty');
    });

    $('.menu_wrapper').show();

    
    window.ishop = x4.getModule('ishopFront');
    
    $('.order_block>.add_tobasket').ishopToBasket({

        basketSumSelector:'.money_block .number',
        basketAllCountSelector:'.basket_block .badge.active',
        syncAddToBasket:false,

        isSKUFunc:function(context){
            return true;
        },

        countDataFunc:function(context){

            t=$(context).closest('.order_block').find('.jq-number input').val();
            return t;
        },
        onFinishedFunc:function(){
            swal({
                title: "Bra jobbat!",
                text: "Produkten läggs i kundvagnen",
                icon: "success",
                button: "ok"
            });


            $('.money_block .number').text(
            accounting.formatMoney($('.money_block .number').text(), {
                format: "%v %s",
                symbol:'',
                thousand: " ",
                precision: 0
            }));



        }
    });



    $('.btn_block>.add_tobasket').ishopToBasket({

        basketSumSelector:'.money_block .number',
        basketAllCountSelector:'.basket_block .badge.active',
        syncAddToBasket:false,
        isSKUFunc:function(context){
            return true;
        },
        countDataFunc:function(context){

            t=$(context).parent().prev().find('.jq-number input').val();

            return t;
        },
        onFinishedFunc:function(){
            swal({
                title: "Bra jobbat!",
                text: "Produkten läggs i kundvagnen",
                icon: "success",
                button: "ok"
            });

            $('.money_block .number').text(
                accounting.formatMoney($('.money_block .number').text(), {
                    format: "%v %s",
                    symbol:'',
                    thousand: " ",
                    precision: 0
                }));

        }
    });

    $('.submitOrder').click(function(e){

        e.preventDefault();

        $('#order').submit();
    })

    $('.sendOrder').click(function(e) {
        e.preventDefault();
        $(this).closest('form').submit();

    });




    /*END Tooltip*/
    
    /*================= menu count ======================*/
    $('.main_menu .submenu').each(function(indx){

        var count = $(this).find('.is-submenu-item').length;

        if ( count > 10 ) {
            $(this).addClass('more');
        }
    });
    
    /*END menu count*/
    
    
});